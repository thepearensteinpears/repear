﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementList : MonoBehaviour
{

    public AchievementListEntry AchievementEntryPrefab;
    public GameObject LeftList;
    public GameObject RightList;

    bool HasLoaded = false;

    bool PutInLeftList = true;

    public void AddAchievement(StorableAchievement achievement)
    {
        var newEntry = GetListEntry();
        newEntry.FillData(achievement);
        PutChildInList(newEntry);
        
    }

    public void AddAchievement(AchievementTriggerMapEntry entry)
    {
        var newEntry = GetListEntry();
        newEntry.FillData(entry);
        PutChildInList(newEntry);
    }

    public void PutChildInList(AchievementListEntry entry)
    {
        if(PutInLeftList)
        {
            entry.transform.SetParent(LeftList.transform, false);
        }
        else
        {
            entry.transform.SetParent(RightList.transform, false);
        }
        PutInLeftList = !PutInLeftList;
    }

    public AchievementListEntry GetListEntry()
    {
        return Instantiate(AchievementEntryPrefab);
    }
    
    public void FillSelf()
    {
        if (HasLoaded)
            return;
        HasLoaded = true;
        foreach(var completed in AchievementManager.Inst.CompletionTracker.CompletedAchievements)
        {
            AddAchievement(completed);
        }
        foreach(var achievement in AchievementManager.Inst.AchievementTriggerMap)
        {
            if(AchievementManager.Inst.CompletionTracker.CompletedAchievements.Find(x=> x.Name == achievement.Name) == null)
            {
                AddAchievement(achievement);
            }
        }
    }

    public void Reload()
    {
        HasLoaded = false;
        foreach(Transform tran in LeftList.transform)
        {
            Destroy(tran.gameObject);
        }
        foreach (Transform tran in RightList.transform)
        {
            Destroy(tran.gameObject);
        }
        FillSelf();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
