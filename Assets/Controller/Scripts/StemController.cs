﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum JumpType
{
    Absolute,
    Relative,
    Count
}

public class StemController : MonoBehaviour
{
    public Rigidbody pearRigidBody;
    public Rigidbody stemRigidBody;
    public float jumpForce;

    public bool IsInContact = false;

    Rigidbody ConnectedBody;
    Vector3 HingePoint = Vector3.zero;

    public bool IsGrabbing = false;
    private SpringJoint CharacterJoint;

    public MergedController Controller { get { return GetComponentInParent<MergedController>(); } }
    public JumpBodySettings Settings { get { return Controller.JumpBodySettings[Controller.JumpBodies.FindIndex(x => x == this)]; } }

    public float GetSpringTensionValue()
    {
        if(CharacterJoint == null)
        {
            return -1;
        }
        return CharacterJoint.currentForce.magnitude / CharacterJoint.breakForce;
    }

    void LateUpdate()
    {
        UpdateState();
    }

    public void UpdateState()
    {

    }

    private void ApplyGrab()
    {
        IsGrabbing = true;
        if (IsInContact && (CharacterJoint == null))
        {
            CharacterJoint = gameObject.AddComponent<SpringJoint>();
            if(ConnectedBody == null)
            {

                CharacterJoint.connectedAnchor = HingePoint;
            }
            else
            {
                CharacterJoint.connectedBody = ConnectedBody;
            }
            CharacterJoint.enableCollision = false;
            
            CharacterJoint.spring = Settings.SpringValue;
            CharacterJoint.breakForce = Settings.BreakForceValue;
            CharacterJoint.damper = Settings.Dampening;
        }
    }

    private void OnJointBreak(float breakForce)
    {
        //TODO: For some reason IsGrabbing is still true after a joint break. Investigate this and fix it
        ReleaseGrab();
    }

    private void ReleaseGrab()
    {
        IsGrabbing = false;
        Destroy(CharacterJoint);
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        ConnectedBody = collision.rigidbody;
        HingePoint = collision.contacts[0].point;
        IsInContact = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        HingePoint = Vector3.zero;
        IsInContact = false;
    }

    public void HandleJump(Rigidbody controlledBody)
    {
        if (!IsInContact)
            return;

        switch (Settings.JumpType)
        {
            case JumpType.Absolute:
                controlledBody.AddForce(Vector3.up * jumpForce);
                break;
            case JumpType.Relative:
                controlledBody.AddForce(pearRigidBody.transform.up * jumpForce);
                break;
            default:
                return;
        }
    }

    public void HandleGrab(bool isGrabButtonDown)
    {
        if(isGrabButtonDown)
        {
            ApplyGrab();
        }
        else
        {
            ReleaseGrab();
        }
    }
}
