﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform Focus;

    MergedController Controller { get { return transform.root.GetComponentInChildren<MergedController>(); } }

    CameraSettings Settings { get { return Controller.CameraSettings; } }

    public bool Legacy_IsCameraLocked
    {
        get
        {
            return Settings.CameraLocked;
        }
        set
        {
            Settings.CameraLocked = value;
        }
    }

	Vector3 FocusPoint;
	Vector3 PrevFocusPoint;
	Vector2 OrbitRotation;

	void Awake()
	{
		FocusPoint = Focus.transform.position;
		PrevFocusPoint = FocusPoint;
		OrbitRotation = Settings.StartingRotation;
	}

	void LateUpdate()
	{
		PrevFocusPoint = FocusPoint;

		var lag = FocusPoint - Focus.position;
		FocusPoint = Vector3.Lerp(FocusPoint, Focus.position, lag.sqrMagnitude * Settings.Responsiveness * Time.deltaTime);

		if(Settings.UseAutoRotate)
		{
			var movement = new Vector2(FocusPoint.x - PrevFocusPoint.x, FocusPoint.z - PrevFocusPoint.z);
			if(movement.magnitude > (0.1f * Time.deltaTime))
			{
				var movementDir = movement.normalized;
				var targetAngle = Vector2.SignedAngle(movementDir, new Vector2(0, 1));
				
				var rotationChange = Settings.RotationSpeed * Time.deltaTime;
				var deltaAngle = Mathf.Abs(Mathf.DeltaAngle(OrbitRotation.y, targetAngle));
				if(deltaAngle < Settings.AlignSmoothRange)
				{
					rotationChange *= (deltaAngle / Settings.AlignSmoothRange);
				}

				OrbitRotation.y = Mathf.MoveTowardsAngle(OrbitRotation.y, targetAngle, rotationChange);
			}
		}
		else if(!Settings.CameraLocked)
		{
			Vector2 input = new Vector2(-Input.GetAxis("Vertical Camera"), Input.GetAxis("Horizontal Camera"));
			if(input.magnitude > 0.01f)
			{
				OrbitRotation += Settings.RotationSpeed * Time.fixedDeltaTime * input;
			}
		}

		OrbitRotation.x = Mathf.Clamp(OrbitRotation.x, Settings.MinVerticalAngle, Settings.MaxVerticalAngle);

		var lookRotation = Quaternion.Euler(OrbitRotation);
		var lookDirection = lookRotation * Vector3.forward;
		var lookPosition = FocusPoint - lookDirection * Settings.Distance;
		transform.SetPositionAndRotation(lookPosition, lookRotation);
	}
}

