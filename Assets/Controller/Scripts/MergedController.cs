﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public enum PlayerState
{
    CanJumpOrGrab,
    InAir,
    Grabbing,
    Count
}

[Serializable]
public class ControllerState
{
    public bool InContact = false;
    public PlayerState PlayerState = PlayerState.CanJumpOrGrab;
}

[Serializable]
public class CameraSettings
{
    [SerializeField, Range(1f, 40f)]
    public float Distance;

    public float Responsiveness;
    public Vector2 StartingRotation;

    [SerializeField, Range(1f, 360f)]
    public float RotationSpeed = 90.0f;

    [SerializeField, Range(-89.0f, 89.0f)]
    public float MinVerticalAngle = -30f;

    [SerializeField, Range(-89.0f, 89.0f)]
    public float MaxVerticalAngle = 60f;

    [SerializeField, Range(0f, 90f)]
    public float AlignSmoothRange = 30f;

    public bool UseAutoRotate;
    public bool CameraLocked;
}

[Serializable]
public class JumpBodySettings
{
    public JumpType JumpType;
    public float SpringValue;
    public float BreakForceValue;
    public float Dampening;
}

[Serializable]
public class MovementSettings
{
    public float MovementForce;
    public float RotationSpeed;
    public float MaxRotationMagnitude;
}

[Serializable]
public class ControllerSettings
{
    public CameraSettings CameraSettings;
    public List<JumpBodySettings> JumpBodySettings = new List<JumpBodySettings>();
    public MovementSettings MovementSettings;
}


public class MergedController : MonoBehaviour
{
    public ControllerSettings ControllerSettings;
    public CameraSettings CameraSettings { get { return ControllerSettings.CameraSettings; } }
    public List<JumpBodySettings> JumpBodySettings { get { return ControllerSettings.JumpBodySettings; } }
    public MovementSettings MovementSettings { get { return ControllerSettings.MovementSettings; } }

    public MeshDeformer MeshDeformer;

    public Rigidbody ControlledBody;

    public ControllerState ControllerState;

    public float GetSpringTensionValue()
    {
        foreach(var jb in JumpBodies)
        {
            if(jb.IsGrabbing)
            {
                return jb.GetSpringTensionValue();
            }
        }
        return -1;
    }

    public List<StemController> JumpBodies = new List<StemController>();

    public bool IsInContact()
    {
        if(ControllerState.InContact)
        {
            return true;
        }
        foreach(var jumpBody in JumpBodies)
        {
            if (jumpBody.IsInContact)
            {
                return true;
            }
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        MeshDeformer.CustomOnCollisionEnterEvent += OnCollisionEnterCustom;
        MeshDeformer.CustomOnCollisionExitEvent += OnCollisionExitCustom;
        MeshDeformer.CustomOnCollisionStayEvent += OnCollisionStayCustom;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateState();
        HandleInput();
    }

    public void UpdateState()
    {
        if(!IsInContact())
        {
            ControllerState.PlayerState = PlayerState.InAir;
        }
        else
        {
            var isGrabbing = false;
            foreach(var jb in JumpBodies)
            {
                isGrabbing |= jb.IsGrabbing;
            }
            if(isGrabbing)
            {
                ControllerState.PlayerState = PlayerState.Grabbing;
            }
            else
            {
                ControllerState.PlayerState = PlayerState.CanJumpOrGrab;
            }
        }
    }

    public void HandleInput()
    {
        HandleMovement();

        HandleGrabInput(Input.GetButton("Grab"));

        switch (ControllerState.PlayerState)
        {
            case PlayerState.CanJumpOrGrab:
                if (Input.GetButtonDown("Jump"))
                {
                    HandleJump();
                }
                else
                {
                    HandleGrabInput(Input.GetButton("Grab"));
                }
                break;

            case PlayerState.Grabbing:
                break;

            case PlayerState.InAir:
                break;

            default:
                break;
        }
    }

    public void HandleMovement()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        if (IsInContact())
        {
            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

            movement = Camera.main.transform.TransformDirection(movement);
            ControlledBody.AddForce(movement * MovementSettings.MovementForce);
        }
        else
        {
            Vector3 movement = new Vector3(moveVertical, 0.0f, -moveHorizontal);
            movement = Camera.main.transform.TransformDirection(movement);
            ControlledBody.angularVelocity += movement * MovementSettings.RotationSpeed;
            if(ControlledBody.angularVelocity.magnitude > MovementSettings.MaxRotationMagnitude)
            {
                ControlledBody.angularVelocity = ControlledBody.angularVelocity.normalized * MovementSettings.MaxRotationMagnitude;
            }
        }
    }

    public void HandleGrabInput(bool isGrabButtonDown)
    {
        for(int i = 0; i < JumpBodies.Count; i++)
        {
            JumpBodies[i].HandleGrab(isGrabButtonDown);
        }
    }

    public void HandleJump()
    {

        for (int i = 0; i < JumpBodies.Count; i++)
        {
            if(JumpBodySettings.Count <= i)
            {
                Debug.LogError("Not enough JumpBodySettings on " + name);
                return;
            }
            JumpBodies[i].HandleJump(ControlledBody);
        }
    }
    
    public void CustomCollision()
    {
    }

    void OnCollisionEnterCustom(Collision collision)
    {
        ControllerState.InContact = true;
    }

    void OnCollisionStayCustom(Collision collision)
    {

    }

    void OnCollisionExitCustom(Collision collision)
    {
        ControllerState.InContact = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //DO NOT USE ME. 
        //The controller has a deformed mesh which requires custom collisions.
        //Use OnCollisionEnterCustom in the same manner you would use this.
    }

    private void OnCollisionStay(Collision collision)
    {
        //DO NOT USE ME. 
        //The controller has a deformed mesh which requires custom collisions.
        //Use OnCollisionStayCustom in the same manner you would use this.
    }

    private void OnCollisionExit(Collision collision)
    {
        //DO NOT USE ME. 
        //The controller has a deformed mesh which requires custom collisions.
        //Use OnCollisionExitCustom in the same manner you would use this.
    }

}
