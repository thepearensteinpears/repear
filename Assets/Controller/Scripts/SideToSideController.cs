﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideToSideController : MonoBehaviour
{
    public float xMin = -25.0f;
    public float xMax = 25.0f;
    public float moveSpeed = 5;
    private bool isDecreasingX = true;

    // Update is called once per frame
    void Update()
    {
        if (isDecreasingX && transform.position.x <= xMin)
        {
            isDecreasingX = false;
        }
        else if (!isDecreasingX && transform.position.x >= xMax)
        {
            isDecreasingX = true;
        }
        float distanceDelta = moveSpeed * Time.deltaTime;
        if (isDecreasingX)
        {
            distanceDelta *= -1;
        }
        
        gameObject.transform.position = new Vector3(transform.position.x + distanceDelta, transform.position.y, transform.position.z);
    }
}
