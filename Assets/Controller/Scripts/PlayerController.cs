﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public bool onTree = true;
	public Vector3 treeLaunchDir;
	public float treeLaunchForce;

    public float movementForce;
    public Rigidbody playerRigidBody;

    #region AchievmentHelpers
    public bool IsOnWall = false;
    public bool IsOnFloor = false;
    public void OnTriggerEnter(Collider other)
    {
        if(other.name.Contains("Wall"))
        {
            IsOnWall = true;
        }
        if (other.name.Contains("Floor"))
        {
            IsOnFloor = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Wall"))
        {
            IsOnWall = false;
        }
        if (other.name.Contains("Floor"))
        {
            IsOnFloor = false;
        }
    }
    #endregion
    void Start()
	{
		onTree = true;
		FindObjectOfType<CameraController>().Legacy_IsCameraLocked = true;
	}

	void Update()
	{
		if(onTree)
		{
			if(Input.GetButtonDown("Jump"))
			{
				onTree = false;
				FindObjectOfType<CameraController>().Legacy_IsCameraLocked = false;
				playerRigidBody.isKinematic = false;
				playerRigidBody.WakeUp();
				playerRigidBody.AddForce(treeLaunchDir * treeLaunchForce, ForceMode.Impulse);
                GameManager.Inst.StartRound();
			}
		}
	}

	void FixedUpdate()
    {
		if(!onTree)
		{
			applyMovement();
		}
	}

    private void applyMovement()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
		movement = Camera.main.transform.TransformDirection(movement);

        playerRigidBody.AddForce(movement * movementForce);
    }
    
}
