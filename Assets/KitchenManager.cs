﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenManager : MonoBehaviour
{
    public GameObject PearAndCameraObject;
    public GameObject BananaAndCameraObject;
    public GameObject PineappleAndCameraObject;

    bool HasInited = false;

    private void Update()
    {
        if(!HasInited && GameManager.Inst != null)
        {
            switch(GameManager.Inst.ControlledFruit)
            {
                case ControllableFruit.Pear:
                    PearAndCameraObject.gameObject.SetActive(true);
                    break;
                case ControllableFruit.Pineapple:
                    PineappleAndCameraObject.gameObject.SetActive(true);
                    break;
                case ControllableFruit.Banana:
                    BananaAndCameraObject.gameObject.SetActive(true);
                    break;
            }
            HasInited = true;
        }
    }

}
