﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroLoadGame : MonoBehaviour
{
    public static IntroLoadGame Inst { get { return FindObjectOfType<IntroLoadGame>(); } }

	public float CutSceneLength;
	public string GameScene;

	void Update()
	{
		/*if(Input.GetButtonDown("Jump"))
		{
			StartIntroCutscene();
		}*/
	}

	public void StartIntroCutscene()
	{
		var animators = FindObjectsOfType<Animator>();
		foreach(var animator in animators)
		{
			animator.enabled = true;
		}

		StartCoroutine(LoadLevel());
	}

	IEnumerator LoadLevel()
	{
		yield return new WaitForSeconds(CutSceneLength);
        RepearSceneManager.Inst.LoadKitchen();
	}
}
