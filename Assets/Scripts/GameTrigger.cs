﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponentInChildren<PlayerController>() != null)
        {
            GameManager.Inst.TriggerEvent(name);
        }
    }
}
