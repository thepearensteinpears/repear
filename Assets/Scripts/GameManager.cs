﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum ControllableFruit
{
    Pear,
    Banana,
    Pineapple,
    Count
}


public class GameManager : MonoBehaviour
{
    public ControllableFruit ControlledFruit = ControllableFruit.Pear;

    public static GameManager Inst { get { return FindObjectOfType<GameManager>(); } }

    public float RoundStartTime = 0f;

    public void TriggerEvent(string eventName)
    {
        AchievementManager.Inst.TriggerAchievement(eventName);
        TriggerGameEvent(eventName);
    }

    public ControllableFruit GetPreviousFruit()
    {
        ControlledFruit = (ControllableFruit)((int)(ControlledFruit - 1 + (int)ControllableFruit.Count) % (int)ControllableFruit.Count);
        if (!HasUnlockedFruit(ControlledFruit))
        {
            return GetPreviousFruit();
        }
        else
        {
            return ControlledFruit;
        }
    }

    public ControllableFruit GetNextFruit()
    {
        ControlledFruit = (ControllableFruit)((int)(ControlledFruit + 1) % (int)ControllableFruit.Count);
        if (!HasUnlockedFruit(ControlledFruit))
        {
            return GetNextFruit();
        }
        else
        {
            return ControlledFruit;
        }
    }

    public int GetUnlockedFruitCount()
    {
        var count = 0;
        for(int i = 0; i < (int)ControllableFruit.Count; i++)
        {
            if(HasUnlockedFruit((ControllableFruit)i))
            {
                count++;
            }
        }
        return count;
    }

    public bool HasUnlockedFruit(ControllableFruit fruit)
    {
        switch (fruit)
        {
            case ControllableFruit.Pear:
                return true;
            case ControllableFruit.Banana:
                return AchievementManager.Inst.HasUnlockedAchievement("Long Yellow Pear");
            case ControllableFruit.Pineapple:
                return AchievementManager.Inst.HasUnlockedAchievement("Great Hair Pear");
            default:
                return false;
        }
    }

    public Oven Oven;

    public void TriggerGameEvent(string eventName)
    {
        switch(eventName)
        {
            case "OpenOven"://I CAN'T BELIEVE I'VE DONE THIS
                FindObjectOfType<Oven>().Open();//OH BOY IT GOT WORSE
                break;
            default:
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Reset Game"))
        {
            RepearSceneManager.Inst.LoadKitchen(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void StartRound()
    {
        ControlledFruit = ControllableFruit.Pear;
        AchievementManager.Inst.ResetCompletion();
        RoundStartTime = Time.time;
    }

    public float GetCurrentRoundTime()
    {
        return Time.time - RoundStartTime;
    }
}
