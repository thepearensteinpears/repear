﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceArea : MonoBehaviour
{
    public float AmountOfForce = 15f;

    private void OnTriggerStay(Collider other)
    {
        var isPlayer = other.GetComponentInChildren<PlayerController>();
        if (isPlayer)
        {
            var rb = other.GetComponentInChildren<Rigidbody>();
            rb.AddForce(gameObject.transform.parent.forward * AmountOfForce);
        }
    }
}
