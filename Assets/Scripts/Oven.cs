﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oven : MonoBehaviour
{
    public void Open()
    {
        var anim = GetComponent<Animator>();
        anim.SetBool("IsOpen", true);
    }

    public void Close()
    {
        var anim = GetComponent<Animator>();
        anim.SetBool("IsOpen", false);
    }
}
