﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RepearSceneManager : MonoBehaviour
{
    public static RepearSceneManager Inst { get { return FindObjectOfType<RepearSceneManager>(); } }

    // Start is called before the first frame update
    void Start()
    {
        var mainScene = SceneManager.GetSceneByName("Main");
        if(!mainScene.isLoaded)
        {
            SceneManager.LoadScene("Main", LoadSceneMode.Additive);
        }
        //LoadIntro();
        LoadKitchen();
    }

    public void LoadKitchen(bool forced = false)
    {
        if (SoundManager.Inst != null)
            SoundManager.Inst.PlayGameMusic();    

        if (SceneManager.GetSceneByName("Intro").isLoaded)
        {
            SceneManager.UnloadSceneAsync("Intro");
        }
        if (!SceneManager.GetSceneByName("Kitchen").isLoaded)
        {
            SceneManager.LoadScene("Kitchen", LoadSceneMode.Additive);
        }
        else if(forced)
        {
            SceneManager.UnloadSceneAsync("Kitchen");
            SceneManager.LoadScene("Kitchen", LoadSceneMode.Additive);
        }
    }

    public void LoadIntro()
    {
        if (SoundManager.Inst != null)
            SoundManager.Inst.PlayGameMusic();

        if (SceneManager.GetSceneByName("Kitchen").isLoaded)
        {
            SceneManager.UnloadSceneAsync("Kitchen");
        }
        if (!SceneManager.GetSceneByName("Intro").isLoaded)
        {
            SceneManager.LoadScene("Intro", LoadSceneMode.Additive);
        }
    }

}
