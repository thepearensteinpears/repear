﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ImpactSound
{
	public float ImpactThreshold;
	public float ImpactRange;
	public float ImpactMinVolume;
	public float InpactMaxVolume;
	public AudioClip Sound;
}

public class MeshDeformer : MonoBehaviour
{
	public float HitForceMultiplier = 0.6f;
	public float ForceOffset = 0.1f;
	public float SpringForce = 0.0f;
	public float Dampening = 13.0f;
	public float PointForceCutoffDist = 0.55f;
	public float PointForceDropOffExponent = 55.0f;
	public float PlaneForceCutoffDist = 0.1f;

	public Color OriginalColor;
	public Color DeformedColor;
	public float FullDeformedColorDist = 0.35f;

	public MeshFilter MeshFilter;
	public MeshCollider MeshCollider;
	public AudioSource SoundAudioSource;

    public int FramesBeforeCollisionChange = 2;
    public int FramesOfSeperation = 0;

	public ImpactSound ImpactSound;

	Mesh Mesh;
	Vector3[] OriginalVertices;
	Vector3[] DeformedVertices;
	Vector3[] VertexVelocities;

    public delegate void CustomOnCollisionEnter(Collision collision);
    public delegate void CustomOnCollisionStay(Collision collision);
    public delegate void CustomOnCollisionLeft(Collision collision);

    public CustomOnCollisionEnter CustomOnCollisionEnterEvent;
    public CustomOnCollisionStay CustomOnCollisionStayEvent;
    public CustomOnCollisionLeft CustomOnCollisionExitEvent;

    void Start()
    {
		Mesh = MeshFilter.mesh;
		OriginalVertices = Mesh.vertices;
		VertexVelocities = new Vector3[OriginalVertices.Length];

		DeformedVertices = new Vector3[OriginalVertices.Length];
		for(int index = 0; index < OriginalVertices.Length; index++)
		{
			DeformedVertices[index] = OriginalVertices[index];
		}
    }

    public bool HadCollisionLastFrame = false;
    public bool HasCollisionThisFrame = false;
    public Collision LastCollision;

    private void FixedUpdate()
    {
        HasCollisionThisFrame = false; //TODO: This seems... bad
    }

    private void LateUpdate()
    {
        HadCollisionLastFrame = HasCollisionThisFrame;
        //Debug.Log(FramesOfSeperation);
    }

    void FireCustomCollisionEvents()
    {
        if (HadCollisionLastFrame && HasCollisionThisFrame)
        {
            if(CustomOnCollisionStayEvent != null)
                CustomOnCollisionStayEvent.Invoke(LastCollision);
            FramesOfSeperation = 0;
        }
        else if (HadCollisionLastFrame && !HasCollisionThisFrame)
        {
            FramesOfSeperation++;
            if(FramesOfSeperation >= FramesBeforeCollisionChange)
            {
                if(CustomOnCollisionExitEvent != null)
                    CustomOnCollisionExitEvent.Invoke(LastCollision);
            }
            else
            {
                HasCollisionThisFrame = true;
            }
        }
        else if(!HadCollisionLastFrame && HasCollisionThisFrame)
        {
            FramesOfSeperation--;
            if(FramesOfSeperation <= -FramesBeforeCollisionChange)
            {
                if(CustomOnCollisionEnterEvent != null)
                    CustomOnCollisionEnterEvent.Invoke(LastCollision);
            }
            else
            {
                HasCollisionThisFrame = false;
            }
        }

    }

    void Update()
    {
        FireCustomCollisionEvents();
		var colors = new Color[DeformedVertices.Length];
		for(int index = 0; index < DeformedVertices.Length; index++)
		{
			var velocity = VertexVelocities[index];
			var displacement = (DeformedVertices[index] - OriginalVertices[index]) * transform.localScale.x;

			var colorBlend = displacement.magnitude / FullDeformedColorDist;
			colors[index] = Color.Lerp(OriginalColor, DeformedColor, colorBlend);

			velocity -= displacement * SpringForce * Time.deltaTime;
			velocity *= 1.0f - (Dampening * Time.deltaTime);

			VertexVelocities[index] = velocity;
			DeformedVertices[index] += velocity * Time.deltaTime;
		}

		Mesh.colors = colors;

		Mesh.vertices = DeformedVertices;
		Mesh.RecalculateNormals();
		Mesh.RecalculateBounds();
		MeshCollider.sharedMesh = null;
		MeshCollider.sharedMesh = Mesh;
	}

	public void ApplyDeformingForce(Vector3 point, Vector3 dir, float force)
	{
		for(int index = 0; index < DeformedVertices.Length; index++)
		{
			var pointToVertex = (DeformedVertices[index] - point) * transform.localScale.x;
			var plane = new Plane(dir, point);

			if(pointToVertex.magnitude < PointForceCutoffDist)
			{
				var attenuatedForce = force / (1.0f + Mathf.Pow(pointToVertex.sqrMagnitude, PointForceDropOffExponent));
				var velocity = attenuatedForce * Time.deltaTime;

				VertexVelocities[index] += pointToVertex.normalized * velocity;
			}
			else if(Mathf.Abs(plane.GetDistanceToPoint(DeformedVertices[index])) < PlaneForceCutoffDist)
			{
				var attenuatedForce = force / (1.0f + Mathf.Pow(pointToVertex.sqrMagnitude, PointForceDropOffExponent));
				var velocity = attenuatedForce * Time.deltaTime;

				VertexVelocities[index] += dir * velocity;
			}
		}
	}



	void OnCollisionEnter(Collision collision)
	{
        HasCollisionThisFrame = true;
        LastCollision = collision;
        
		var contacts = new ContactPoint[collision.contactCount];
		collision.GetContacts(contacts);

		foreach(var contact in contacts)
		{
			var forcePoint = transform.InverseTransformPoint(contact.point + (-contact.normal * ForceOffset));
			var forceDir = transform.InverseTransformDirection(contact.normal);

			if(collision.impulse.magnitude > 0.5f)
			{
				ApplyDeformingForce(forcePoint, forceDir, collision.impulse.magnitude * HitForceMultiplier);
			}
		}

		if(ImpactSound != null && ImpactSound.Sound != null && collision.impulse.magnitude > ImpactSound.ImpactThreshold)
		{
			var volume = Mathf.Clamp01((collision.impulse.magnitude - ImpactSound.ImpactThreshold) / ImpactSound.ImpactRange);
			SoundAudioSource.volume = volume;
			SoundAudioSource.clip = ImpactSound.Sound;
			SoundAudioSource.Play();
		}
	}
}
