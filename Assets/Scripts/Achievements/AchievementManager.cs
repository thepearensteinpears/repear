﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[Serializable]
public class AchievementTriggerMapEntry
{
    public string Name;
    public string Description_CURRENTLY_NOT_USED;
    public Sprite Sprite_CURRENTLY_NOT_USED;
    public bool IsTriggered;
}

[Serializable]
public class CompletionTracker
{
    public List<StorableAchievement> CompletedAchievements = new List<StorableAchievement>();
}


[Serializable]
public class StorableAchievement
{
    public string Name;
    public float Time;
}

public class AchievementManager : MonoBehaviour
{

    public AchievementToast Toaster;


    public static AchievementManager Inst { get { return FindObjectOfType<AchievementManager>(); } }

    public List<AchievementTriggerMapEntry> AchievementTriggerMap = new List<AchievementTriggerMapEntry>()
    {

    };
    public CompletionTracker CompletionTracker = new CompletionTracker();

    public void TriggerAchievement(string achievementString)
    {
        for(int i = 0; i <AchievementTriggerMap.Count; i++)
        {
            var achieve = AchievementTriggerMap[i];
            if(achieve.Name == achievementString)
            {
                achieve.IsTriggered = true;
                return;
            }
        }
    }

    public void ResetCompletion()
    {
        for (int i = 0; i < AchievementTriggerMap.Count; i++)
        {
            var achieve = AchievementTriggerMap[i].IsTriggered = false;
        }
    }


    public void Start()
    {
        LoadAchievements();
    }

    public void Update()
    {
        CheckAchievements();
    }

    public void CheckAchievements()
    {
        var toDestroy = new List<AchievementTriggerMapEntry>();
        foreach(var achievement in AchievementTriggerMap)
        {
            var completedEntry = CompletionTracker.CompletedAchievements.Find(x => x.Name == achievement.Name);
            var hasGottenAchievementInFasterTime = 
                completedEntry != null && 
                (completedEntry.Time < GameManager.Inst.GetCurrentRoundTime());

            var isAchievementReadyToShow = achievement.IsTriggered;

            if (!hasGottenAchievementInFasterTime && isAchievementReadyToShow)
            {
                CompleteAchievement(achievement);
            }
        }
    }

    public void CompleteAchievement(AchievementTriggerMapEntry achievement)
    {
        Toaster.QueueAchievement(achievement);
        var foundAchieve = CompletionTracker.CompletedAchievements.Find(x => x.Name == achievement.Name); //Convert this to a dictionary. The Tech Debt is REAL
        if (foundAchieve != null)
        {
            foundAchieve.Time = GameManager.Inst.GetCurrentRoundTime();
        }
        else
        {
            CompletionTracker.CompletedAchievements.Add(new StorableAchievement() { Name = achievement.Name, Time = GameManager.Inst.GetCurrentRoundTime() });
        }
        SaveAchievementProgress();
    }

    const string achievementPath = "Assets/Resources/achievements.txt";

    public void SaveAchievementProgress()
    {
        var path = achievementPath;
        var json = JsonUtility.ToJson(CompletionTracker);

        File.WriteAllText(path, json);
        
    }

    public void LoadAchievements()
    {
        CompletionTracker.CompletedAchievements.Clear();
        var path = achievementPath;

        StreamReader reader = new StreamReader(path);
        var tracker = JsonUtility.FromJson<CompletionTracker>(reader.ReadToEnd());
        if (tracker != null) 
        {
            CompletionTracker = tracker;
        }
        reader.Close();
    }

    public void ResetAchievements()
    {
        var path = achievementPath;
        File.WriteAllText(path,"");
        LoadAchievements();
    }

    public bool HasUnlockedAchievement(string name)
    {
        return CompletionTracker.CompletedAchievements.Find(x => x.Name == name) != null;
    }
}
