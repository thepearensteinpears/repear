﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementToast : MonoBehaviour
{
    public Text ShownAchievementName;
    public string ShownAchievementDescription_CURRENTLY_NOT_USED;
    public Image ShownAchievementImage_CURRENTLY_NOT_USED;

    public Animator Animator;

    List<AchievementTriggerMapEntry> AchievementQueue = new List<AchievementTriggerMapEntry>();

    public void QueueAchievement(AchievementTriggerMapEntry achievementTriggerMapEntry)
    {
        AchievementQueue.Add(achievementTriggerMapEntry);   
    }

    // Update is called once per frame
    void Update()
    {
        if(Animator.GetCurrentAnimatorStateInfo(0).IsName("Hidden") && AchievementQueue.Count > 0)
        {
            var achievement = AchievementQueue[0];
            AchievementQueue.RemoveAt(0);
            ShowAchievement(achievement);
        }
    }

    public void ShowAchievement(AchievementTriggerMapEntry achievement)
    {
        ShownAchievementName.text = achievement.Name;
        ShownAchievementDescription_CURRENTLY_NOT_USED = achievement.Description_CURRENTLY_NOT_USED;
        ShownAchievementImage_CURRENTLY_NOT_USED.sprite = achievement.Sprite_CURRENTLY_NOT_USED;
        PlayAnimation();
    }

    public void PlayAnimation()
    {
        Animator.Play("ShowAchievement");
    }



}
