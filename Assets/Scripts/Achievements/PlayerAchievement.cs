﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAchievement : MonoBehaviour
{
    public static PlayerController Controller { get { return GameObject.FindObjectOfType<PlayerController>(); } }//I HATE IT

    //float InAirRequirement = 5f;
    float InAirTimer = 0;

    void Update()
    {
        if(Controller!=null)
        {
            if (Controller.IsOnFloor || Controller.onTree)
            {
                InAirTimer = 0;
            }
            else
            {
                InAirTimer += Time.deltaTime;
            }
            if (InAirTimer >= 5f)
            {
                AchievementManager.Inst.TriggerAchievement("AirTime");
            }
        }
    }

    public void CheckAchievements()
    {

    }
    
}
