﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Outro : MonoBehaviour
{
	public Camera OutroCamera;
	public string RestartGameScene;
	public Transform PlayerSpawnLocation;

    public GameObject Pineapples;
    public GameObject Pears;
    public GameObject Bananas;
    public GameObject ShowOnPlay;
    

    public bool HasPlayed = false;

    public void Play()
    {
        if (HasPlayed)
            return;
        HasPlayed = true;
        Camera.main.enabled = false;
        OutroCamera.tag = "MainCamera";
        OutroCamera.enabled = true;
        ShowOnPlay.SetActive(true);

        switch (GameManager.Inst.ControlledFruit)
        {
            case ControllableFruit.Pear:
                Pears.SetActive(true);
                break;
            case ControllableFruit.Banana:
                Bananas.SetActive(true);
                break;
            case ControllableFruit.Pineapple:
                Pineapples.SetActive(true);
                break;
            default:
                break;
        }

        

        var players = FindObjectsOfType<PlayerController>();
        foreach (var player in players)
        {
            player.gameObject.SetActive(false);
        }
        //PlayerController activePlayer = null;
        /*
            if (player.gameObject.set)
            {
                activePlayer = player;
                break;
            }
        }
        
        if (activePlayer != null)
        {
            var rigidbodies = activePlayer.GetComponentsInChildren<Rigidbody>();
            foreach (var rigidbody in rigidbodies)
            {
                rigidbody.isKinematic = true;
            }

            var colliders = activePlayer.GetComponentsInChildren<Collider>();
            foreach (var collider in colliders)
            {
                collider.enabled = false;
            }

            var components = activePlayer.GetComponentsInChildren<MonoBehaviour>();
            foreach (var component in components)
            {
                if (component is Renderer || component is MeshFilter)
                {

                }
                else
                {
                    component.enabled = false;
                }
            }

            activePlayer.transform.SetPositionAndRotation(PlayerSpawnLocation.transform.position, PlayerSpawnLocation.transform.rotation);
        }*/
    }


    void Update()
	{
	}
}
