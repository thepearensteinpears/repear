﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroUIManager : MonoBehaviour
{
    public GameObject Pear;
    public GameObject Banana;
    public GameObject Pineapple;
    public GameObject Pear2;
    public GameObject Banana2;
    public GameObject Pineapple2;

    
    public Button PlayButton;
    public Button AchievementBackButton;
    public GameObject FruitSelectionButtons;


    private void Start()
    {
        PlayButton.Select();
    }

    private void Update()
    {
        if(GameManager.Inst != null)
        {
            FruitSelectionButtons.gameObject.SetActive(GameManager.Inst.GetUnlockedFruitCount() > 1);
        }
        else
        {
            FruitSelectionButtons.gameObject.SetActive(false);
        }
    }

    public GameObject MainMenuPanels;
    public GameObject AchievementMenuPanels;

    public void OnPlayButtonPressed()
    {
        IntroLoadGame.Inst.StartIntroCutscene();
    }

    public void OnAchievementButtonPressed()
    {
        MainMenuPanels.SetActive(false);
        AchievementMenuPanels.SetActive(true);
        AchievementBackButton.Select();
    }

    public void OnBackButtonPressed()
    {
        AchievementMenuPanels.SetActive(false);
        MainMenuPanels.SetActive(true);
        PlayButton.Select();
    }

    public void OnResetAchievementsPressed()
    {
        AchievementManager.Inst.ResetAchievements();
    }

    public void NextFruit()
    {
        GameManager.Inst.GetNextFruit();
        SetFruit(GameManager.Inst.ControlledFruit);
    }

    public void PreviousFruit()
    {
        GameManager.Inst.GetPreviousFruit();
        SetFruit(GameManager.Inst.ControlledFruit);
    }

    public void SetFruit(ControllableFruit controllable)
    {
        switch(controllable)
        {
            case ControllableFruit.Banana:
                Pear.gameObject.SetActive(false);
                Pear2.gameObject.SetActive(false);
                Banana.gameObject.SetActive(true);
                Banana2.gameObject.SetActive(true);
                Pineapple.gameObject.SetActive(false);
                Pineapple2.gameObject.SetActive(false);
                break;
            case ControllableFruit.Pear:
                Pear.gameObject.SetActive(true);
                Pear2.gameObject.SetActive(true);
                Banana.gameObject.SetActive(false);
                Banana2.gameObject.SetActive(false);
                Pineapple.gameObject.SetActive(false);
                Pineapple2.gameObject.SetActive(false);
                break;
            case ControllableFruit.Pineapple:
                Pear.gameObject.SetActive(false);
                Pear2.gameObject.SetActive(false);
                Banana.gameObject.SetActive(false);
                Banana2.gameObject.SetActive(false);
                Pineapple.gameObject.SetActive(true);
                Pineapple2.gameObject.SetActive(true);
                break;


        }
    }

}
