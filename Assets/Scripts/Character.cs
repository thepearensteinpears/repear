﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public float MaxHealingRate = 1f;
    public float HealingRateDeltaPerSecond = .05f;
    float CurrentHealingRate = 0f;
    public bool IsHealing = false;

    public void OnTriggerEnter(Collider other)
    {
        if(other.name == "HealingZone")
        {
            SetIsHealing(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.name == "HealingZone")
        {
            SetIsHealing(false);
        }
    }

    public void SetIsHealing(bool isHealing)
    {
        IsHealing = isHealing;
        if(IsHealing == false)
        {
            CurrentHealingRate = 0f;
            UpdateHealingRate(CurrentHealingRate);
        }
    }

    public void HealSelf()
    {
        UpdateHealingRate(CurrentHealingRate);
        CurrentHealingRate = Mathf.Min(MaxHealingRate, CurrentHealingRate + HealingRateDeltaPerSecond * Time.deltaTime);
        AchievementManager.Inst.TriggerAchievement("Repear");
    }

    public void UpdateHealingRate(float newRate)
    {
        var deforms = GetComponents<MeshDeformer>();
        foreach (MeshDeformer deform in deforms)
        {
            deform.SpringForce = newRate;
        }
    }

    public void Update()
    {
        if(IsHealing)
        {
            HealSelf();
        }
    }
}
