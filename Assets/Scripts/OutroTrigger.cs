﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutroTrigger : MonoBehaviour
{
	public Outro Outro;

	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponentInChildren<PlayerController>() != null)
		{
            Outro.Play();
		}
	}
}
