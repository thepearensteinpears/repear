﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementState
{
    StoppedFromRotating = 0,
    StoppedFromCollision,
    MovingForwards, 
    MovingBackwards,
    Rotating,
    Count
}


public class RoombaMovement : MonoBehaviour
{
    public float StopTimeInSeconds = .5f;
    public float StopTimer = 0f;

    public float RotationSpeedDegreesPerSecond = 50;
    private float TargetAngle = 0f;
    private bool RotatingClockwise = false;

    public float BackwardTime = .2f;
    private float BackwardTimer = 0f;

    public float MovementSpeed = 10f;

    private MovementState MovementState = MovementState.StoppedFromRotating;
    public void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.name.Contains("Floor"))
        {
            SetState(MovementState.StoppedFromCollision);
        }
    }

    public void StartMovingBackwards()
    {
        BackwardTimer = BackwardTime;
    }

    public void StopMovement()
    {
        StopTimer = StopTimeInSeconds;
    }

    public void SetState(MovementState movementState)
    {
        MovementState = movementState;
        switch (MovementState)
        {
            case MovementState.StoppedFromRotating:
            case MovementState.StoppedFromCollision:
                StopMovement();
                break;
            case MovementState.Rotating:
                StartRotating();
                break;
            case MovementState.MovingBackwards:
                StartMovingBackwards();
                break;
            default:
                break;
        }
    }

    public void StartRotating()
    {
        float degrees = Random.Range(-75, 75);
        RotatingClockwise = degrees >= 0;
        TargetAngle = transform.localEulerAngles.y + degrees;
    }

    public void UpdateBackwards()
    {
        transform.Translate(-transform.forward * MovementSpeed * Time.deltaTime, Space.World);
        UpdateBackwardsTimer();
    }

    public void UpdateBackwardsTimer()
    {
        BackwardTimer -= Time.deltaTime;
        if(BackwardTimer <= 0)
        {
            SetState(MovementState.Rotating);
        }
    }

    public void UpdateRotation()
    {
        transform.Rotate(new Vector3(0, 1, 0), (Time.deltaTime * RotationSpeedDegreesPerSecond * (RotatingClockwise ? 1f : -1f)));
        if ((RotatingClockwise && (transform.localEulerAngles.y >= TargetAngle)) || (!RotatingClockwise && (transform.localEulerAngles.y <= TargetAngle)))
        {
            SetState(MovementState.StoppedFromRotating);
        }
    }

    public void UpdateStopTimer()
    {
        StopTimer -= Time.deltaTime;
        if(StopTimer <= 0)
        {
            SetState(MovementState == MovementState.StoppedFromCollision ? MovementState.MovingBackwards : MovementState.MovingForwards);
        }

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(MovementState);
        switch(MovementState)
        {
            case MovementState.StoppedFromRotating:
            case MovementState.StoppedFromCollision:
                UpdateStopTimer();
                break;
            case MovementState.Rotating:
                UpdateRotation();
                break;
            case MovementState.MovingForwards:
                transform.Translate(transform.forward * MovementSpeed * Time.deltaTime, Space.World);
                break;
            case MovementState.MovingBackwards:
                UpdateBackwards();
                break;
            default:
                break;
        }
    }
}
