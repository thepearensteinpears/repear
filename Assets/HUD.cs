﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public MergedController Controller;

    //TODO: This should be a list and each icon should be an instance of a class. Then when we assign a controlled body, we can instantiate all of the icons we need and associate them with specific stemcontrollers
    //      Then they could get their own data instead of going through Controller
    public Image GrabIconImage;

    private void Update()
    {
        //TODO: Only shows feedback for one joint
        var colorVal = Controller.GetSpringTensionValue();
        if(colorVal == -1)
        {
            GrabIconImage.transform.localScale = Vector3.one;
            if(Controller.JumpBodies[0].IsInContact)
            {
                GrabIconImage.color = Color.yellow;
            }
            else
            {
                GrabIconImage.color = Color.white;
            }
        }
        else
        {
            GrabIconImage.color = Color.Lerp(Color.green, Color.red, colorVal);
            GrabIconImage.transform.localScale = Vector3.one * (1f + 1f * colorVal);
        }
    }
}
