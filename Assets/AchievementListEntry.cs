﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementListEntry : MonoBehaviour
{
    public Image Image;
    public Text UnlockedTitle;
    public Text LockedTitle;
    public Text Description;
    public Text Time;

    public Sprite LockedSprite;

    public void FillData(StorableAchievement storable)
    {
        var entry = AchievementManager.Inst.AchievementTriggerMap.Find(x => x.Name == storable.Name);
        Image.sprite = entry.Sprite_CURRENTLY_NOT_USED;
        UnlockedTitle.text = "<b>" + entry.Name + "</b>";
        Description.text = entry.Description_CURRENTLY_NOT_USED;
        Time.text = "Best Time: \n" + storable.Time.ToString("0.00");

        LockedTitle.gameObject.SetActive(false);
        UnlockedTitle.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        Time.gameObject.SetActive(true);
    }

    public void FillData(AchievementTriggerMapEntry entry)
    {
        Image.sprite = LockedSprite;
        LockedTitle.text = entry.Name;

        LockedTitle.gameObject.SetActive(true);
        UnlockedTitle.gameObject.SetActive(false);
        Description.gameObject.SetActive(false);
        Time.gameObject.SetActive(false);
    }

    
}
