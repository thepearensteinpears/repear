﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Inst { get { return FindObjectOfType<SoundManager>(); } }

    public AudioClip IntroMusic;
    public AudioClip GameMusic;

    AudioSource Source { get { return GetComponent<AudioSource>(); } }
    
    public void PlayIntroMusic()
    {
        Source.Stop();
        Source.loop = true;
        Source.clip = IntroMusic;
        Source.Play();
    }

    public void PlayGameMusic()
    {
        Source.Stop();
        Source.loop = true;
        Source.clip = GameMusic;
        Source.Play();
    }

}
